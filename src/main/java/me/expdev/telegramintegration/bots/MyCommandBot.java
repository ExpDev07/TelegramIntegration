package me.expdev.telegramintegration.bots;

import me.expdev.telegramintegration.commands.MyTestCommand;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

/*
 * Project created by ExpDev
 */

/**
 * A testing bot
 */
public class MyCommandBot extends TelegramLongPollingCommandBot {

    private String username;
    private String token;

    public MyCommandBot(String username, String token) {
        this.username = username;
        this.token = token;

        // Instance of the test command
        MyTestCommand testCommand = new MyTestCommand();

        // Registering our command
        register(testCommand);

        // Making sure the command is known
        registerDefaultAction((absSender, message) -> {
            SendMessage commandUnknownMessage = new SendMessage();
            commandUnknownMessage.setChatId(message.getChatId());
            commandUnknownMessage.setText("The command '" + message.getText() + "' is not known by this bot. Here comes some help ");
            try {
                absSender.sendMessage(commandUnknownMessage);
            } catch (TelegramApiException e) {
                BotLogger.error("MYCOMMANDBOT", e);
            }
            // Executing command with no arguments
            testCommand.execute(absSender, message.getFrom(), message.getChat(), new String[] {});
        });
    }

    @Override
    public void processNonCommandUpdate(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            System.out.println(message);

            // Making sure there actually is a message
            if (message.hasText()) {
                SendMessage echoMessage = new SendMessage();
                echoMessage.setChatId(message.getChatId());
                echoMessage.setText("Hey here is your message: " + message.getText());

                try {
                    sendMessage(echoMessage);
                } catch (TelegramApiException e) {
                    BotLogger.error("MYCOMMANDBOT", e);
                }
            }
        }
    }

    @Override
    public String getBotUsername() {
        return this.username;
    }

    @Override
    public String getBotToken() {
        return this.token;
    }

    @Override
    public void onClosing() {
        System.out.println("Closing " + getBotToken());
    }
}
