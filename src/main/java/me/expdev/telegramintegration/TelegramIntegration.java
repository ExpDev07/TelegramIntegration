package me.expdev.telegramintegration;

/*
 * Project created by ExpDev
 */

import me.expdev.telegramintegration.bots.MyCommandBot;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.logging.BotLogger;

public class TelegramIntegration {

    // Our singleton instance of the main class
    private static TelegramIntegration i = null;

    // The instance to the API
    private TelegramBotsApi api = null;

    /**
     * Executes when the program runs
     */
    public static void main(String[] args) {
        // Initializing api and registering bots
        TelegramIntegration main = getInstance();
    }

    /**
     * Singleton construction for main class
     */
    private TelegramIntegration() {
        i = this;

        // Important as of new versions!: https://github.com/rubenlagus/TelegramBots/wiki/How-To-Update#to-version-242
        ApiContextInitializer.init();

        this.api = new TelegramBotsApi();

        // Registering bots
        try {
            getApi().registerBot(new MyCommandBot("mariuscommandsbot_bot", "redacted"));
            System.out.println("Registered bot");
        } catch (TelegramApiRequestException e) {
            BotLogger.error("MYCOMMANDBOT", e);
        }
    }

    /**
     * @return Single instance of main class
     */
    public static TelegramIntegration getInstance() {
        if (i == null) {
            return new TelegramIntegration();
        }
        return i;
    }

    /**
     * @return The Telegram Bots API instance
     */
    public TelegramBotsApi getApi() {
        return api;
    }
}
