package me.expdev.telegramintegration.commands;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commands.BotCommand;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;

/*
 * Project created by ExpDev
 */

/**
 * A simple /mytest command
 */
public class MyTestCommand extends BotCommand {

    public MyTestCommand() {
        super("mytest", "Says nice test!");
    }

    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] args) {
        String userName = chat.getUserName();
        if (userName == null || userName.isEmpty()) {
            userName = user.getFirstName() + " " + user.getLastName();
        }

        String yes = "Yes, the test is working, ";
        StringBuilder messageTextBuilder = new StringBuilder(yes).append(userName);
        if (args != null && args.length > 0) {
            messageTextBuilder.append("\n");
            messageTextBuilder.append("Thank you so much for your kind words:\n");
            messageTextBuilder.append(String.join(" ", args));
        }

        SendMessage answer = new SendMessage();
        answer.setChatId(chat.getId().toString());
        answer.setText(messageTextBuilder.toString());

        try {
            absSender.sendMessage(answer);
        } catch (TelegramApiException e) {
            BotLogger.error("MYTESTCOMMAND", e);
        }
    }
}
